// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4013,x:33746,y:32897,varname:node_4013,prsc:2|diff-1304-RGB,emission-5732-OUT;n:type:ShaderForge.SFN_Color,id:1304,x:32447,y:32489,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_VertexColor,id:2198,x:32447,y:32726,varname:node_2198,prsc:2;n:type:ShaderForge.SFN_Lerp,id:6029,x:33119,y:32852,varname:node_6029,prsc:2|A-8021-OUT,B-1127-OUT,T-3523-OUT;n:type:ShaderForge.SFN_FragmentPosition,id:6300,x:31241,y:33149,varname:node_6300,prsc:2;n:type:ShaderForge.SFN_Append,id:2520,x:31727,y:33173,varname:node_2520,prsc:2|A-6300-X,B-6300-Y;n:type:ShaderForge.SFN_Tex2dAsset,id:2474,x:31981,y:33025,ptovrint:False,ptlb:noise,ptin:_noise,varname:node_2474,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:3201,x:32247,y:33126,varname:node_3201,prsc:2,ntxv:0,isnm:False|UVIN-1593-OUT,MIP-2521-OUT,TEX-2474-TEX;n:type:ShaderForge.SFN_Vector1,id:8021,x:32447,y:32654,varname:node_8021,prsc:2,v1:0;n:type:ShaderForge.SFN_Clamp01,id:3523,x:32738,y:33089,varname:node_3523,prsc:2|IN-9661-OUT;n:type:ShaderForge.SFN_RemapRange,id:9661,x:32519,y:33126,varname:node_9661,prsc:2,frmn:0,frmx:1,tomn:-1.5,tomx:3|IN-3201-R;n:type:ShaderForge.SFN_Vector1,id:2521,x:32067,y:33401,varname:node_2521,prsc:2,v1:6;n:type:ShaderForge.SFN_Vector1,id:7556,x:31241,y:33362,varname:node_7556,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:1593,x:31939,y:33272,varname:node_1593,prsc:2|A-2520-OUT,B-2035-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2035,x:31614,y:33445,ptovrint:False,ptlb:node_2035,ptin:_node_2035,varname:node_2035,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Tex2d,id:7906,x:32186,y:32837,varname:node_7906,prsc:2,ntxv:0,isnm:False|UVIN-7577-UVOUT,MIP-5490-OUT,TEX-2474-TEX;n:type:ShaderForge.SFN_TexCoord,id:4468,x:31808,y:32774,varname:node_4468,prsc:2,uv:0;n:type:ShaderForge.SFN_Panner,id:7577,x:31988,y:32774,varname:node_7577,prsc:2,spu:-0.05,spv:0|UVIN-4468-UVOUT;n:type:ShaderForge.SFN_Multiply,id:1127,x:32851,y:32876,varname:node_1127,prsc:2|A-2198-R,B-3966-OUT;n:type:ShaderForge.SFN_Clamp01,id:3966,x:32601,y:32892,varname:node_3966,prsc:2|IN-4957-OUT;n:type:ShaderForge.SFN_RemapRange,id:4957,x:32447,y:32892,varname:node_4957,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:3|IN-7906-R;n:type:ShaderForge.SFN_Vector1,id:5490,x:31958,y:32944,varname:node_5490,prsc:2,v1:5;n:type:ShaderForge.SFN_Color,id:4775,x:33119,y:33043,ptovrint:False,ptlb:IllumColor,ptin:_IllumColor,varname:node_4775,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5073529,c2:1,c3:0.9592292,c4:1;n:type:ShaderForge.SFN_Multiply,id:9421,x:33345,y:32902,varname:node_9421,prsc:2|A-6029-OUT,B-4775-RGB;n:type:ShaderForge.SFN_NormalVector,id:6622,x:32539,y:33355,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:661,x:32714,y:33355,varname:node_661,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-6622-OUT;n:type:ShaderForge.SFN_Clamp01,id:2623,x:33172,y:33429,varname:node_2623,prsc:2|IN-8030-OUT;n:type:ShaderForge.SFN_Multiply,id:1765,x:33364,y:33429,varname:node_1765,prsc:2|A-2623-OUT,B-4726-RGB;n:type:ShaderForge.SFN_Color,id:4726,x:32739,y:33580,ptovrint:False,ptlb:TopLight,ptin:_TopLight,varname:node_25,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.6764706,c2:0.8795132,c3:1,c4:1;n:type:ShaderForge.SFN_RemapRange,id:8030,x:32930,y:33272,varname:node_8030,prsc:2,frmn:-1,frmx:1,tomn:0,tomx:1|IN-661-OUT;n:type:ShaderForge.SFN_Add,id:5732,x:33466,y:33097,varname:node_5732,prsc:2|A-1765-OUT,B-9421-OUT;proporder:1304-2474-2035-4775-4726;pass:END;sub:END;*/

Shader "vio/lit_artefact" {
    Properties {
        _Color ("Color", Color) = (0,0,0,1)
        _noise ("noise", 2D) = "white" {}
        _node_2035 ("node_2035", Float ) = 1
        _IllumColor ("IllumColor", Color) = (0.5073529,1,0.9592292,1)
        _TopLight ("TopLight", Color) = (0.6764706,0.8795132,1,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform sampler2D _noise; uniform float4 _noise_ST;
            uniform float _node_2035;
            uniform float4 _IllumColor;
            uniform float4 _TopLight;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = _Color.rgb;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float4 node_983 = _Time + _TimeEditor;
                float2 node_7577 = (i.uv0+node_983.g*float2(-0.05,0));
                float4 node_7906 = tex2Dlod(_noise,float4(TRANSFORM_TEX(node_7577, _noise),0.0,5.0));
                float2 node_1593 = (float2(i.posWorld.r,i.posWorld.g)*_node_2035);
                float4 node_3201 = tex2Dlod(_noise,float4(TRANSFORM_TEX(node_1593, _noise),0.0,6.0));
                float3 emissive = ((saturate((i.normalDir.g*0.5+0.5))*_TopLight.rgb)+(lerp(0.0,(i.vertexColor.r*saturate((node_7906.r*4.0+-1.0))),saturate((node_3201.r*4.5+-1.5)))*_IllumColor.rgb));
/// Final Color:
                float3 finalColor = diffuse + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform sampler2D _noise; uniform float4 _noise_ST;
            uniform float _node_2035;
            uniform float4 _IllumColor;
            uniform float4 _TopLight;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 diffuseColor = _Color.rgb;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
