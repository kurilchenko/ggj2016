﻿using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class NetworkTransformWriter : NetworkBehaviour
{
    public List<NetworkTransformPlus> targetTransforms = new List<NetworkTransformPlus>();

    public void AddNetworkTransform(NetworkTransformPlus networkTransform)
    {
        targetTransforms.RemoveAll(tt => tt == networkTransform);
        targetTransforms.Add(networkTransform);
    }

    [Server]
    public void WriteSyncState(NetworkBehaviourPlus.StateComponent syncState)
    {
        //Debug.Log(">>" + syncState.targetName + "; M: " + gameObject.name);

        var networkTransform = targetTransforms.Find(nt => nt.targetTransform.name == syncState.targetName);

        networkTransform.stateHistory.Add(syncState);
    }

}
