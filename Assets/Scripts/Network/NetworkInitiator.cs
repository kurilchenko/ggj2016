﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class NetworkInitiator : MonoBehaviour
{

    public enum Status
    {
        Client,
        Host
    }

    public Status status;
    public string targetIPAddress;
    public int targetNetworkPort;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    void Start ()
    {
        // Choose whether your a server or a network.

        //status = Status.Client;
/*
#if UNITY_EDITOR
        status = Status.Host;
#endif
*/
        SceneManager.LoadScene(1);
    }
	
}
