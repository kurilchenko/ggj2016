﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]
public class NetworkRigidbody : NetworkBehaviourPlus
{
    protected new Rigidbody rigidbody;

    public override float GetNetworkSendInterval()
    {
        return 0f;
    }

    public override int GetNetworkChannel()
    {
        return 1; // Unreliable.
    }

    public override StateComponent GetNewState()
    {
        var newState = base.GetNewState();
        newState.timestamp = NetworkTransport.GetNetworkTimestamp();
        newState.position = transform.position;
        newState.rotation = transform.rotation;
        newState.velocity = rigidbody.velocity;
        newState.isKinematic = rigidbody.isKinematic;
        newState.angularVelocity = rigidbody.angularVelocity;

        return newState;
    }

    protected override void Start()
    {
        base.Start();

        rigidbody = GetComponent<Rigidbody>();

        if (!isServer)
        {
            rigidbody.isKinematic = true;
        }
    }

    protected override void Update()
    {
        base.Update();

        if (!isServer)
        {
            SyncState();
        }
        else
        {
            tickStateHistory.Add(GetNewState());
        }
    }

    protected override void NetworkUpdate()
    {
        base.NetworkUpdate();

        if (isServer)
        {
            RpcAddHistory(tickStateHistory.ToArray());
            tickStateHistory.Clear();
        }
    }

    [ClientRpc(channel = 1)]
    protected void RpcAddHistory(StateComponent[] buffer)
    {
        if (isServer)
            return;

        stateHistory.AddRange(buffer);
        ShortenHistory();
    }

    protected override void SyncState()
    {
        var states = GetInterStates();

        if (states.prevState.timestamp != -1)
        {
            rigidbody.isKinematic = true;
            transform.position = Vector3.Lerp(states.prevState.position, states.targetState.position, states.portion);
            transform.rotation = Quaternion.Slerp(states.prevState.rotation, states.prevState.rotation, states.portion);
        }
        else if (stateHistory.Count > 0)
        {
            int lastStateIndex = stateHistory.Count - 1;

            rigidbody.isKinematic = stateHistory[lastStateIndex].isKinematic;
            rigidbody.velocity = stateHistory[lastStateIndex].velocity;
            rigidbody.angularVelocity = stateHistory[lastStateIndex].angularVelocity;
        }
    }

}
