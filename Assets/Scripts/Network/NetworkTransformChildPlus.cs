﻿using UnityEngine;
using System.Collections;

public class NetworkTransformChildPlus : NetworkTransformPlus
{
    public Transform target;

    protected override void OnEnable()
    {
        if (target == null)
        {
            Destroy(this);
            return;
        }

        // Set targetTransform to a customly defined target before it's set in the base class to the transform in this gameObject.
        targetTransform = target;

        base.OnEnable();
    }

    protected override void Update()
    {
        base.Update();
    }

    /*
    public Transform targetTransform;

    protected override void OnEnable()
    {
        if (targetTransform == null)
        {
            Destroy(this);
            return;
        }

        _targetTransform = targetTransform;

        base.OnEnable();
    }

    public override StateComponent GetNewState()
    {
        var newState = base.GetNewState();
        newState.position = _targetTransform.localPosition;
        newState.rotation = _targetTransform.localRotation;

        return newState;
    }
    */

    /*
    protected override void SyncTransform()
    {
        if (interpolation == 0)
        {
            _targetTransform.localPosition = syncState.position;
            _targetTransform.localRotation = syncState.rotation;
        }
        else
        {
            _targetTransform.localPosition = Vector3.Lerp(_targetTransform.localPosition, syncState.position, Time.deltaTime / interpolation);
            _targetTransform.localRotation = Quaternion.Lerp(_targetTransform.localRotation, syncState.rotation, Time.deltaTime / interpolation);
        }
    }
    */
}
