﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NetworkRoomManager : NetworkManager
{
    NetworkInitiator initiator;

    void Start()
    {
        initiator = FindObjectOfType<NetworkInitiator>();

        if (initiator == null || initiator.status == NetworkInitiator.Status.Host)
        {
            Debug.Log("Role: Host");

            CreateLocalRoom(initiator);
        }
        else if (initiator.status == NetworkInitiator.Status.Client)
        {
            Debug.Log("Role: Client");

            JoinLocalRoom(initiator);
        }
    }

    void CreateLocalRoom(NetworkInitiator initiator)
    {
        Debug.Log("Trying to start a new server at IP: " + Network.player.ipAddress + ", port: " + networkPort);

        StartHost();
    }

    void JoinLocalRoom(NetworkInitiator initiator)
    {
        Debug.Log("Trying to connect to a local server at IP: " + initiator.targetIPAddress + ", port: " + initiator.targetNetworkPort);

        networkAddress = initiator.targetIPAddress;
        networkPort = initiator.targetNetworkPort;
  
        StartClient();
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);

        Debug.Log("Client has connected to a local server");
        Debug.Log("Creating a new local player...");

        /*
        if (initiator != null && initiator.status == NetworkInitiator.Status.Client)
        {
            ClientScene.RegisterPrefab(playerPrefab);
            ClientScene.AddPlayer(0);
        }
        */

        ClientScene.RegisterPrefab(playerPrefab);
        ClientScene.AddPlayer(0);
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        base.OnServerAddPlayer(conn, playerControllerId);
    }

    public override void OnStartHost()
    {
        base.OnStartHost();

        Debug.Log("Local server has been created");
    }

}
