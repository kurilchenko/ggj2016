﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

[NetworkSettings(channel = 1)]
public class NetworkTransformPlus : NetworkBehaviourPlus
{

    public Rigidbody targetRigidbody { get; protected set; }
    public Transform targetTransform { get; protected set; }

    public override float GetNetworkSendInterval()
    {
        return 0f;
    }

    public override int GetNetworkChannel()
    {
        return 1; // Unreliable.
    }

    public override StateComponent GetNewState()
    {
        var newState = base.GetNewState();

        newState.targetName = targetTransform.name;
        newState.position = targetTransform.position;
        newState.rotation = targetTransform.rotation;      

        if (targetRigidbody != null)
        {
            newState.velocity = targetRigidbody.velocity;
            newState.isKinematic = targetRigidbody.isKinematic;
            newState.angularVelocity = targetRigidbody.angularVelocity;
        }

        return newState;
    }

    protected override void Start()
    {
        base.Start();

        if (!isServer && targetRigidbody != null)
        {
            targetRigidbody.isKinematic = true;
        }
    }

    protected override void UpdateOnClient()
    {
        base.UpdateOnClient();

        SyncState();
    }

    protected override void UpdateOnServer()
    {
        base.UpdateOnServer();

        tickStateHistory.Add(GetNewState());
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        if (targetTransform == null)
        {
            targetTransform = transform;
        }

        if (targetRigidbody == null)
        {
            targetRigidbody = targetTransform.GetComponent<Rigidbody>();
        }
    }

    protected override void NetworkUpdate()
    {
        base.NetworkUpdate();

        if (isServer)
        {
            //syncTickStateHistory = tickStateHistory;
            //RpcAddHistory(tickStateHistory.ToArray());
            //syncTickStateHistory[0]

            var buffer = new NetworkBehaviourPlus.StateComponentBuffer();
            buffer.states = tickStateHistory.ToArray();
            syncTickStateHistory = buffer;

            //RpcAddHistory(buffer.states);

            tickStateHistory.Clear();
        }
    }

    protected override void SyncState()
    {
        var states = GetInterStates();

        if (states.prevState.timestamp != -1)
        {
            if (targetRigidbody != null)
            {
                targetRigidbody.isKinematic = true;
            }

            targetTransform.position = Vector3.Lerp(states.prevState.position, states.targetState.position, states.portion);
            targetTransform.rotation = Quaternion.Slerp(states.prevState.rotation, states.prevState.rotation, states.portion);
        }
        else if (stateHistory.Count > 0)
        {
            int lastStateIndex = stateHistory.Count - 1;

            if (targetRigidbody != null)
            {
                targetRigidbody.isKinematic = stateHistory[lastStateIndex].isKinematic;
                targetRigidbody.velocity = stateHistory[lastStateIndex].velocity;
                targetRigidbody.angularVelocity = stateHistory[lastStateIndex].angularVelocity;
            }
        }
    }

}
