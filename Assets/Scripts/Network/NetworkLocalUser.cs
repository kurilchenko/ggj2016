﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace Tests
{
    [RequireComponent(typeof(NetworkIdentity))]
    public class NetworkLocalUser : NetworkBehaviour
    {
        public static NetworkLocalUser Instance
        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }
                else
                {
                    foreach (var user in FindObjectsOfType<NetworkLocalUser>())
                    {
                        var netIdentity = user.GetComponent<NetworkIdentity>();

                        if (netIdentity != null && netIdentity.isLocalPlayer && netIdentity.localPlayerAuthority)
                        {
                            _instance = user;
                        }
                    }
    
                    return _instance;
                }
            }
        }

        public static NetworkIdentity LocalUserNetworkIdentity
        {
            get
            {
                return Instance.GetComponent<NetworkIdentity>();
            }
        }

        static NetworkLocalUser _instance;
    }
}


