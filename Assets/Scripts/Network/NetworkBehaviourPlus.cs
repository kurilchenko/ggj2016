﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class NetworkBehaviourPlus : NetworkBehaviour
{
    [System.Serializable]
    public struct StateComponent
    {
        public int timestamp;
        //public PureStateParams parameters;
        public Vector3 position;
        public Quaternion rotation;
        public bool isKinematic;
        public Vector3 velocity;
        public Vector3 angularVelocity;
        public string targetName;
    }

    public struct StateComponentBuffer
    {
        public StateComponent[] states;
    }

    [System.Serializable]
    public struct InterpStates
    {
        public StateComponent prevState;
        public StateComponent targetState;
        public float portion;
    }

    public static int renderDelay = 250; // Preferably to adjust from a dedicated script that would gather info about latency and add a buffer time (~100ms).
    public static NetworkClient serverClient;

    public float networkUpdateRate = 30f;
    [HideInInspector]
    public List<StateComponent> stateHistory = new List<StateComponent>();

    [SyncVar(hook = "OnUpdateTickStateHistory")]
    protected StateComponentBuffer syncTickStateHistory;

    protected List<StateComponent> tickStateHistory = new List<StateComponent>();

    public virtual StateComponent GetNewState()
    {
        var newState = new StateComponent();
        newState.timestamp = NetworkTransport.GetNetworkTimestamp();

        return newState;
    }

    public static int GetServerDelayTimeMS(int timestamp)
    {
        NetworkConnection connection;
        byte error;

        if (serverClient != null)
        {
            connection = serverClient.connection;
        }
        else
        {
            connection = NetworkManager.singleton.client.connection;
        }

        return NetworkTransport.GetRemoteDelayTimeMS(
            connection.hostId,
            connection.connectionId,
            timestamp,
            out error);
    }

    /// <summary>
    /// Get two states from stateHistory that are suitable for interpolating between.
    /// </summary>
    /// <returns>Returns two states from stateHistory with a portion (from 0 to 1 for Lerp), 
    /// prevState is older than current network time minus delay time and targetState one is more recent. 
    /// If there's no suitable statess in stateHistory then state returns with timestamps -1.</returns>
    public virtual InterpStates GetInterStates()
    {
        InterpStates states = new InterpStates();

        states.prevState.timestamp = -1;
        states.targetState.timestamp = -1;

        if (stateHistory.Count == 0)
            return states;

        int prevStateIndex = -1;
        int targetStateIndex = -1;
        int lastStateTimestamp = stateHistory[stateHistory.Count - 1].timestamp;
        int lastStateDelay = GetServerDelayTimeMS(lastStateTimestamp); // Get a delay between the last state.
        // QUESTION: why do I add an additional delay in form of "renderDelay" and don't go with just the server delay?
        // QUESTION: and shouldn't I have an abs value so I wouldn't get a negative delay.
        int renderTimestamp = lastStateTimestamp - (renderDelay - lastStateDelay); // Get a timestamp that was renderDelay MS ago on a server.

        // Find a state that is behind renderTimestamp.
        prevStateIndex = stateHistory.FindLastIndex(ts => ts.timestamp < renderTimestamp);
        // Find a state that is more recent than renderTimestamp.
        targetStateIndex = stateHistory.FindIndex(ts => ts.timestamp > renderTimestamp);

        // If there are suitable states to interpolate between.
        if (prevStateIndex != -1 && targetStateIndex != -1)
        {
            states.prevState = stateHistory[prevStateIndex];
            states.targetState = stateHistory[targetStateIndex];

            // Get a portion between prevState and targetState. Basically, what Mathf.InverseLerp() does.
            states.portion = (float)(renderTimestamp - states.prevState.timestamp) / (float)(states.targetState.timestamp - states.prevState.timestamp);
        }

        return states;
    }

    protected virtual void Awake()
    {

    }

    protected virtual void Start()
    {

    }

    protected virtual void OnEnable()
    {
        //InvokeRepeating("NetworkUpdate", 0f, 1f / networkUpdateRate);
        StartCoroutine(CoNetworkUpdate());
    }

    protected virtual void OnDisable()
    {
        //CancelInvoke("NetworkUpdate");
        StopCoroutine(CoNetworkUpdate());
    }

    protected IEnumerator CoNetworkUpdate()
    {
        while(true)
        {
            NetworkUpdate();

            yield return new WaitForSeconds(1f / networkUpdateRate);
        }   
    }


    protected virtual void NetworkUpdate()
    {
        
    }

    protected virtual void Update()
    {
        if (isServer)
        {
            UpdateOnServer();
        }
        else
        {
            UpdateOnClient();
        }


        if (isLocalPlayer)
        {
            UpdateOnLocalPlayer();
        }
        else
        {
            UpdateOnAlien();
        }
    }

    protected virtual void UpdateOnServer()
    {
        
    }

    protected virtual void UpdateOnClient()
    {

    }

    protected virtual void UpdateOnLocalPlayer()
    {

    }

    protected virtual void UpdateOnAlien()
    {

    }

    protected virtual void SyncState()
    {

    }

    protected void ShortenHistory()
    {
        int newOldestTimestamp = stateHistory[stateHistory.Count - 1].timestamp - 500;
        stateHistory.RemoveAll(ts => ts.timestamp < newOldestTimestamp);
    }

    protected virtual void OnUpdateTickStateHistory(StateComponentBuffer buffer)
    {
        if (isServer)
            return;

        stateHistory.AddRange(buffer.states);
        ShortenHistory();

        // TODO: implement delta encoding.
    }

    [ClientRpc]
    protected void RpcSetPositionInstantly(Vector3 position)
    {
        transform.position = position;
    }

    [ClientRpc]
    protected void RpcSetRotationInstantly(Quaternion rotation)
    {
        transform.rotation = rotation;
    }

    [ClientRpc]
    protected void RpcSetScaleInstantly(Vector3 scale)
    {
        transform.localScale = scale;
    }

    [ClientRpc]
    public void RpcSetTransformInstantly(Vector3 position, Quaternion rotation, Vector3 scale)
    {
        transform.position = position;
        transform.rotation = rotation;
        transform.localScale = scale;
    }



}
