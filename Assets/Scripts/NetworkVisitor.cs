﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

[RequireComponent(typeof(Visitor))]
public class NetworkVisitor : NetworkBehaviourPlus
{
	public LayerMask vrCullingMask;
	public LayerMask realCullingMask;

    Visitor visitor;
    Tests.VisitorTool[] tools;

    public void RequestSetPosition(NetworkInstanceId netId, Vector3 position)
    {
        CmdSetPosition(netId, position);
    }

    [Command]
    void CmdSetPosition(NetworkInstanceId netId, Vector3 position)
    {
        var target = NetworkServer.FindLocalObject(netId);

        var obtainable = target.GetComponent<NetworkObtainableItem>();
        obtainable.SrvrSetPosition(position);
    }

    [Command]
    void CmdSetRotation(NetworkInstanceId netId, Quaternion rotation)
    {
        var target = NetworkServer.FindLocalObject(netId);

        var obtainable = target.GetComponent<NetworkObtainableItem>();
        obtainable.SrvrSetRotation(rotation);
    }

    [Command]
    void CmdSetAuthority(NetworkInstanceId targetNetId)
    {
        var target = NetworkServer.FindLocalObject(targetNetId);

        var networkHandle = target.GetComponent<Tests.NetworkHandle>();
        networkHandle.SetAuthority(netId);
    }

    protected override void Start()
    {
        base.Start();

        visitor = GetComponent<Visitor>();
        visitor.isInLocalControl = isLocalPlayer;
        visitor.Install();

		if (isServer) {
			FindObjectOfType<VisitorSpotManager> ().AddVisitor (visitor);
			visitor.cameraRig.GetComponentInChildren<Camera> ().cullingMask = realCullingMask;
		} else {
			visitor.cameraRig.GetComponentInChildren<Camera> ().cullingMask = vrCullingMask;
		}

        tools = GetComponents<Tests.VisitorTool>();

        if (!isLocalPlayer)
        {
            foreach (var tool in tools)
            {
                tool.enabled = false;
            }
        }

        gameObject.name = "Visitor_" + netId;
    }

}
