﻿using UnityEngine;
using System.Collections;

public class HackDrawingHand : MonoBehaviour
{
    public Transform drawOrigin;
    public SixenseHand hand
    {
        get
        {
            return GetComponent<SixenseHand>();
        }
    }
}
