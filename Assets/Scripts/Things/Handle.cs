﻿using UnityEngine;
using System.Collections;

namespace Tests
{
    public class Handle : ObtainableThing
    {
        public Transform parent;

        Vector3 initLocalPos;
        Quaternion initLocalRot;

        public Vector3 diffPos;
        //Vector3 diffRot;

        Vector3 lastPos;
        // TODO: lastRot;

        protected override void Start()
        {
            base.Start();

            if (parent == null)
            {
                parent = transform.parent;
            }

            lastPos = transform.position;
            diffPos = transform.position - parent.position;
            //diffRot = transform.eulerAngles - parent.eulerAngles;

            transform.parent = parent.parent;
        }

        void Update()
        {
            if(transform.position != lastPos)
            {
                var currentPos = transform.position;
                //var currentRot = transform.rotation;

                var targetParentPos = transform.position - diffPos;
                // TODO: doesn't work correctly. Needs revisiting.
                //var targetParentRot = Quaternion.Euler(transform.eulerAngles - diffRot);

                parent.position = targetParentPos;
                //parent.transform.position = Vector3.Lerp(parent.position, targetParentPos, Time.deltaTime * 15f);
                // TODO: why are movements so choppy?
                //parent.transform.position = targetParentPos;

                //transform.position = currentPos;
                //transform.rotation = currentRot;
            }

            lastPos = transform.position;
        }
    }
}


