﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NetworkObtainableItem : NetworkBehaviour
{
    [SyncVar(hook ="OnSyncPositionChange")]
    public Vector3 syncPosition;

    [SyncVar]
    public Quaternion syncRotation;
    
    //bool updatePos;

    [Server]
    public void SrvrSetPosition(Vector3 position)
    {
        //syncPosition = position;
        RpcSetPosition(position);
    }

    [Server]
    public void SrvrSetRotation(Quaternion rotation)
    {
        RpcSetRotation(rotation);
    }

    void Start()
    {
        /*
        if (isServer)
        {
            syncPosition = transform.position;
            syncRotation = transform.rotation;
        }
        */
    }
	
	void OnSyncPositionChange(Vector3 position)
    {
        //updatePos = true;
        Debug.Log(position);
    }

    [ClientRpc]
    void RpcSetPosition(Vector3 position)
    {
        transform.position = position;
    }

    [ClientRpc]
    void RpcSetRotation(Quaternion rotation)
    {
        transform.rotation = rotation;
    }

    /*
    void Update()
    {
        if (!updatePos)
            return;

        transform.position = syncPosition;

        updatePos = false;
    }
    */
}
