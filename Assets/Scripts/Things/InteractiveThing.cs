﻿using UnityEngine;
using System.Collections;

namespace Tests
{
    public class InteractiveThing : MonoBehaviour
    {
        public delegate void EventHandler();
        public event EventHandler OnHoverEvent;
        public event EventHandler OnUnhoverEvent;
        public event EventHandler OnSelectEvent;

        public virtual void OnHover(GameObject sender)
        {
            Debug.Log(gameObject + " hover");
        }
        public virtual void OnUnhover(GameObject sender)
        {
            Debug.Log(gameObject + " unhover");
        }
        public virtual void OnSelect(GameObject sender) { }

        protected virtual void Start() { }
        protected virtual void Awake() { }
        protected virtual void OnEnable() { }
        protected virtual void OnDisable() { }
    }

}
