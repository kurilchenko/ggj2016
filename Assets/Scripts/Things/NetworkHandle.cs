﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

namespace Tests
{
    [RequireComponent(typeof(Handle))]
    public class NetworkHandle : NetworkBehaviour
    {
        [SyncVar]
        NetworkInstanceId syncAuthNetId;

        [Server]
        public void SetAuthority(NetworkInstanceId netId)
        {
            syncAuthNetId = netId;
        }

        [Server]
        public void RemoveAuthority()
        {
            syncAuthNetId = new NetworkInstanceId();
        }

        void OnEnable()
        {
            GetComponent<Handle>().OnPickUpEvent += OnPickUp;
            GetComponent<Handle>().OnDropOffEvent += OnDropOff;
        }

        void OnPickUp()
        {
            
        }

        void OnDropOff()
        {
            
        }
    }
}

