﻿using UnityEngine;
using System.Collections;

namespace Tests
{
    public class ObtainableThing : InteractiveThing
    {
        public event EventHandler OnPickUpEvent;
        public event EventHandler OnDropOffEvent;

        public float holdDistance = 1f;

        public virtual void OnPickUp()
        {
            if (OnPickUpEvent != null)
            {
                OnPickUpEvent.Invoke();
            }
        }

        public virtual void OnDropOff()
        {
            if (OnDropOffEvent != null)
            {
                OnDropOffEvent.Invoke();
            }
        }

    }

}
