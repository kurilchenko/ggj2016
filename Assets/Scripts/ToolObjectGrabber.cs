﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ToolObjectGrabber : Tests.VisitorTool 
{
	public string grabWithTag;
	public EnterExitSensor sensor;
	public List<GameObject> nearObjects = new List<GameObject>();

	public Vector3 hackVelocity;
	Vector3 prevPos;

	SixenseHand hand;
	GameObject targetInHand;
	Transform prevTargetInHandParent;

	void Start()
	{
		sensor.registerNamesTagsTypes.Add (grabWithTag);
		hand = GetComponent<SixenseHand> ();
	}

	void OnEnable()
	{
		sensor.OnSensorEnter += OnSensorEnter;
		sensor.OnSensorExit += OnSensorExit;
	}

	void OnDisable()
	{
		sensor.OnSensorEnter -= OnSensorEnter;
		sensor.OnSensorExit -= OnSensorExit;		
	}

	void OnSensorEnter(GameObject target)
	{
		nearObjects.Remove (target);
		nearObjects.Add (target);
	}

	void OnSensorExit(GameObject target)
	{
		nearObjects.Remove (target);
	}

	void Update()
	{
		hackVelocity = sensor.transform.position - prevPos;
		prevPos = sensor.transform.position;

		if (!IsControllerActive(hand.m_controller))
			return;

		if (hand.m_controller.GetButtonDown (SixenseButtons.TRIGGER)) 
		{
			targetInHand = nearObjects.Count > 0 ? nearObjects [0] : null;

			if (targetInHand != null) {
				if (targetInHand.GetComponentInParent<ToolObjectGrabber>() == null)
				{
					targetInHand.GetComponent<Rigidbody> ().isKinematic = true;
					prevTargetInHandParent = targetInHand.transform.parent;
					targetInHand.transform.parent = transform;
				}
			}
		}

		if (hand.m_controller.GetButtonUp (SixenseButtons.TRIGGER) && targetInHand != null) 
		{
			targetInHand.transform.parent = prevTargetInHandParent;

			targetInHand.GetComponent<Rigidbody> ().isKinematic = false;
			targetInHand.GetComponent<Rigidbody> ().velocity = hackVelocity * 100f;
		}
	}

	public void DropTarget()
	{
		if (targetInHand == null)
			return;

		targetInHand.transform.parent = prevTargetInHandParent;
		targetInHand.GetComponent<Rigidbody> ().isKinematic = false;	
	}

	bool IsControllerActive(SixenseInput.Controller controller)
	{
		return (controller != null && controller.Enabled && !controller.Docked);
	}

}
