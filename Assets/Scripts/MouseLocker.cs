﻿using UnityEngine;
using System.Collections;

public class MouseLocker : MonoBehaviour
{
	void Start ()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
	}
}
