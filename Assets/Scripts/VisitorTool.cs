﻿using UnityEngine;
using System.Collections;

namespace Tests
{
    public class VisitorTool : MonoBehaviour
    {

        public Visitor visitor
        {
            get
            {
                return GetComponent<Visitor>();
            }
        }

        protected virtual void Start() { }

        protected virtual void OnEnable() { }

        protected virtual void OnDisable() { }

    }
}
