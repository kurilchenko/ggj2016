﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NetworkTransformSimple : NetworkBehaviour
{
	Rigidbody rigibody;

	void Start () 
	{
		rigibody = GetComponent<Rigidbody> ();

		if (isServer) {
			InvokeRepeating ("SendTransform", 0f, 1 / 60f);
		}
	}

	void SendTransform()
	{
		RpcSetTransform (transform.position, transform.rotation, transform.localScale);
	}

	void Update()
	{
		if (!isServer) {
			rigibody.isKinematic = true;		
		}
	}

	[ClientRpc]
	void RpcSetTransform(Vector3 position, Quaternion rotation, Vector3 localScale)
	{
		if (isServer)
			return;
		
		transform.position = position;
		transform.rotation = rotation;
		transform.localScale = localScale;
	}

}
