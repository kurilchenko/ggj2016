﻿using UnityEngine;
using System.Collections;

public class Visitor : MonoBehaviour
{
    public bool isInLocalControl;
    public float interactionTime = 0.5f;
    public Transform cameraRig;
    //[HideInInspector]
    //public Sight sight;
    public GameObject avatar;
    public GameObject lastTarget;

    float elapesdTimeOnTarget;
    float targetInteractionTime;
    new Camera camera;
    AudioListener audioListener;

    public void Install()
    {
        SetLocalControl(isInLocalControl);
    }

    void Awake()
    {
        camera = cameraRig.GetComponentInChildren<Camera>();
        audioListener = camera.GetComponent<AudioListener>();
    }

    void Start()
    {
        //sight = GetComponent<Sight>();

        Install();
    }

    void SetLocalControl(bool value)
    {
        camera.enabled = value;
        audioListener.enabled = value;
        // Activate mouse camera only when in local control and with a VR headset turned off.
        //cameraRig.GetComponent<MouseCameraControl>().enabled = value && !UnityEngine.VR.VRSettings.enabled;

        if (avatar != null)
        {
            avatar.SetActive(!value);
        }
    }

    void Update()
    {
        #if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.F12))
        {
            var currentAntiAliasing = QualitySettings.antiAliasing;
            QualitySettings.antiAliasing = 16;
            Application.CaptureScreenshot(Time.time.ToString()+".png", 4);
            QualitySettings.antiAliasing = currentAntiAliasing;
        }
        #endif
    }

}
