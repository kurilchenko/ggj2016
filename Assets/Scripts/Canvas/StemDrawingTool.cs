﻿using UnityEngine;
using System.Collections;

namespace Tests
{
    public class StemDrawingTool : VisitorTool
    {
        public DrawingCanvas[] canvas;
        public HackDrawingHand[] hands;

        Vector3 endPoint;

        protected override void Start()
        {
            base.Start();

            // Call the method a bit later so 'visitor.isInLocalControl' will be set before the call.
            Invoke("SetupCanvasEditor", Time.deltaTime);
        }

        void SetupCanvasEditor()
        {
            if (!visitor.isInLocalControl)
                return;

            CanvasManager.Setup();

            foreach (var canvas in CanvasManager.Instance.canvases)
            {
                canvas.editor = gameObject;
            }
        }

        void Update()
        {
            if (canvas == null)
                return;

            for (var i = 0; i < hands.Length; i++)
            {
                var hand = hands[i].hand;

                if (!IsControllerActive(hand.m_controller))
                    return;

                if (hand.m_controller.GetButton(SixenseButtons.TRIGGER))
                {
					var grabber = hand.GetComponent<ToolObjectGrabber> ();

					if (grabber == null || grabber.nearObjects.Count == 0) {
						var localPoint = canvas [i].transform.InverseTransformPoint (hands [i].drawOrigin.position);
						var localNormal = canvas [i].transform.InverseTransformVector (hands [i].drawOrigin.forward);
						canvas [i].AddPoint (localPoint, localNormal);
					}
                }
                else
                {
                    canvas[i].StopPainting();
                }

				if (hand.m_controller.GetButton (SixenseButtons.BUMPER)) 
				{
					canvas [i].Undo ();
				}
            }
        }

        bool IsControllerActive(SixenseInput.Controller controller)
        {
            return (controller != null && controller.Enabled && !controller.Docked);
        }
    }
}
