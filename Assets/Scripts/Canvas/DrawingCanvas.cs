﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Tests
{
    public class DrawingCanvas : MonoBehaviour
    {
        // Stored in local positions to the transform in order to be translated correctly when the canvas is being moved.
        public List<Vector3> points = new List<Vector3>();
        public List<Vector3> normals = new List<Vector3>();
        public uint id;
        public float step = 0.01f;
        public float thickness = 0.01f;
        public Transform pointer;
        public uint editorId { get; protected set; }

        GameObject _editor;

        public GameObject editor
        {
            get
            {
                return _editor;
            }
            set
            {
                if (_editor != null)
                {
                    Debug.LogWarning("Re-setting 'editor' from " + _editor.name + " to " + value.name + ".");
                }
                
                _editor = value;

                var editorNetBehaviour = _editor.GetComponent<UnityEngine.Networking.NetworkBehaviour>();
                if (editorNetBehaviour != null)
                {
                    editorId = editorNetBehaviour.netId.Value;
                }
            }
        }

        public virtual bool AddPoint(Vector3 localPoint, Vector3 localNormal)
        {
            // If the new point is closer to the last point than the step distance then don't add it.
            if (points.Count > 0 && Vector3.Distance(localPoint, points[points.Count - 1]) < step)
                return false;

            points.Add(localPoint);
            normals.Add(localNormal);

            return true;
        }

        public virtual void SetHover(Vector3 localPoint)
        {
            UpdatePointer(localPoint);
        }

        public virtual void Unhover()
        {
            if (pointer != null)
                pointer.gameObject.SetActive(false);
        }

        public virtual void StopPainting()
        {

        }

        public virtual void Undo()
        {

        }

        public virtual void Reset()
        {
            points.Clear();
        }

        void OnEnable()
        {
            if (pointer != null)
                pointer.gameObject.SetActive(false);
            CanvasManager.Add(this);
        }

        void OnDisable()
        {
            CanvasManager.Remove(this);
        }

        void Update()
        {
        }

        void UpdatePointer(Vector3 localPosition)
        {
            if (pointer == null)
                return;

            pointer.gameObject.SetActive(true);
            pointer.position = transform.TransformPoint(localPosition);
            pointer.localScale = Vector3.one * thickness * 1.4f;
        }

    }

}
