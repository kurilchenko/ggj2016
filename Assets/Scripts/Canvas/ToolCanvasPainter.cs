﻿using UnityEngine;
using System.Collections;

namespace Tests
{
    [RequireComponent(typeof(Visitor))]
    public class ToolCanvasPainter : VisitorTool
    {
        public DrawingCanvas canvas;

        Transform origin;
        Vector3 endPoint;
        RaycastHit hit;

        protected override void Start()
        {
            base.Start();

            origin = visitor.cameraRig.GetComponentInChildren<Camera>().transform;

            // Call the method a bit later so 'visitor.isInLocalControl' will be set before the call.
            Invoke("SetupCanvasEditor", Time.deltaTime);
        }

        void SetupCanvasEditor()
        {
            if (!visitor.isInLocalControl)
                return;

            CanvasManager.Setup();

            foreach (var canvas in CanvasManager.Instance.canvases)
            {
                canvas.editor = gameObject;
            }
        }

        void Update()
        {
            UpdateHit();

            if (Input.GetKey(KeyCode.Mouse0))
            {
                UpdateCanvasDraw();
            }
            else if (canvas != null)
            {
                canvas.StopPainting();
            }

            if (canvas != null)
            {
                var localPoint = canvas.transform.InverseTransformPoint(hit.point);
                canvas.SetHover(localPoint);

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    canvas.Undo();
                }
            }
        }

        void UpdateHit()
        {
            Physics.Raycast(origin.position, origin.forward, out hit, 100f);
            endPoint = hit.collider != null ? hit.point : origin.position + origin.forward * 100f;

            var prevCanvas = canvas;

            if (hit.collider != null)
            {
                canvas = hit.rigidbody != null ? hit.rigidbody.GetComponent<DrawingCanvas>() : hit.collider.GetComponent<DrawingCanvas>();
            }
            else
            {
                canvas = null;
            }

            //canvas = hit.collider != null ? hit.collider.GetComponent<DrawingCanvas>() : null;

            if (prevCanvas != canvas && prevCanvas != null)
            {
                prevCanvas.SendMessage("Unhover", SendMessageOptions.DontRequireReceiver);
                prevCanvas.SendMessage("StopPainting", SendMessageOptions.DontRequireReceiver);
            }
        }

        void UpdateCanvasDraw()
        {
            if (hit.collider == null)
                return;

            //canvas = hit.collider.GetComponent<TestPainter>();

            if (canvas == null)
                return;

            var localPoint = canvas.transform.InverseTransformPoint(hit.point);
            var localNormal = canvas.transform.InverseTransformVector(hit.normal);

            Debug.DrawLine(hit.point, hit.point + hit.normal);

            canvas.AddPoint(localPoint + localNormal * 0.005f, localNormal);
        }
    }
}
