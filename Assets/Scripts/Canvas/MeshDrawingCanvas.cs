﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Tests
{
    public class MeshDrawingCanvas : DrawingCanvas
    {
        public MarkerStroke targetStroke;
        public Material material;
        public List<MarkerStroke> strokes = new List<MarkerStroke>();
        public List<MarkerStroke> ownStrokes = new List<MarkerStroke>();
        public delegate void StrokeCreation(MarkerStroke stroke);
        public delegate void StrokeAddSegment(MarkerStroke stroke, Vector3 localPoint, Vector3 localNormal);
        public delegate void UndoStroke(MarkerStroke stroke);
        public event StrokeCreation OnCreateStroke;
        public event StrokeAddSegment OnAddSegment;
        public event UndoStroke OnUndo;

        public override bool AddPoint(Vector3 localPoint, Vector3 localNormal)
        {
            bool added = base.AddPoint(localPoint, localNormal);

            if (!added)
                return false;

            if (targetStroke == null)
            {
                targetStroke = CreateStroke(thickness, true);
            }

            // Add the point and normal from the world space.
            targetStroke.AddSegment(transform.TransformPoint(localPoint), transform.TransformVector(localNormal));

            if (OnAddSegment != null)
            {
                OnAddSegment.Invoke(targetStroke, localPoint, localNormal);
            }
            
            return added;
        }

        public MarkerStroke CreateStroke(MarkerStrokeData strokeData)
        {
            if (strokeData.ownerId == NetworkLocalUser.Instance.netId.Value)
                return null;

            var stroke = CreateStroke(strokeData.thickness, false);

            stroke.data = strokeData;

            //strokes.Add(stroke);

            return stroke;
        }

        public MarkerStroke CreateStroke(float thickness, bool own)
        {
            var stroke = PrepareNewStroke();

            stroke.thickness = thickness;
            stroke.data = GetStrokeData(stroke);

            strokes.Add(stroke);

            if (own)
            {
                ownStrokes.Add(stroke);
                if (OnCreateStroke != null)
                {
                    OnCreateStroke.Invoke(stroke);
                }
            }

            return stroke;
        }

        MarkerStroke PrepareNewStroke()
        {
            var stroke = new GameObject("stoke", typeof(MarkerStroke)).GetComponent<MarkerStroke>();

            stroke.data.id = stroke.gameObject.GetInstanceID();
            stroke.gameObject.name = stroke.data.id.ToString();
            stroke.transform.position = transform.position;
            stroke.GetComponent<MeshRenderer>().sharedMaterial = material;
            stroke.transform.parent = transform;

            return stroke;
        }

        public MarkerStrokeData[] GetData()
        {
            var data = new MarkerStrokeData[strokes.Count];

            for (var i = 0; i < strokes.Count; i++)
            {
                data[i] = strokes[i].data;
            }

            return data;
        }

        public MarkerStrokeData GetStrokeData(MarkerStroke stroke)
        {
            var strokeData = new MarkerStrokeData();

            strokeData.id = stroke.gameObject.GetInstanceID();
            strokeData.ownerId = editorId;
            strokeData.thickness = stroke.thickness;
            strokeData.points = stroke.GetPoints();

            return strokeData;
        }
        
        public void RequestUndoStroke(uint ownerId, int strokeId)
        {
            if (ownerId == editorId)
                return;

            var stroke = strokes.Find(s => s.data.ownerId == ownerId && s.data.id == strokeId);

            if (stroke == null)
            {
                Debug.Log("Couldn't remove stroke with id:" + strokeId + ", ownerId:" + ownerId + ", because there isn't one" );
                return;
            }

            strokes.Remove(stroke);

            Destroy(stroke.gameObject);
        }

        public override void StopPainting()
        {
            base.StopPainting();

            targetStroke = null;
        }

        public override void Undo()
        {
            base.Undo();

            if (ownStrokes.Count == 0)
                return;


            var lastStroke = ownStrokes[ownStrokes.Count - 1];

            if (OnUndo != null)
            {
                OnUndo.Invoke(lastStroke);
            }
            
            Destroy(lastStroke.gameObject);
            strokes.Remove(lastStroke);
            ownStrokes.Remove(lastStroke);
        }

        public void Make(MarkerStrokeData[] data)
        {
            Debug.Log("Make stokes using " + data.Length + " strokes from the server");

            for (int i = 0; i < data.Length; i++)
            {
                var strokeData = data[i];

                Debug.Log("Stroke #" + i + ", made by user #" + strokeData.ownerId);

                var stroke = PrepareNewStroke();
                stroke.data = strokeData;
                stroke.thickness = strokeData.thickness;
                stroke.gameObject.name = stroke.data.id.ToString();

                strokes.Add(stroke);

                for (var pi = 0; pi < strokeData.points.Length; pi++)
                {
                    var point = strokeData.points[i];
                    var normal = strokeData.normals[i];
                    stroke.AddSegment(point, normal, true);
                }

                stroke.UpdateMesh();

                Debug.Log("---");
            }
        }

        public override void Reset()
        {
            base.Reset();

            foreach (var stroke in ownStrokes)
            {
                Destroy(stroke.gameObject);
            }

            ownStrokes.Clear();
        }

    }
}
