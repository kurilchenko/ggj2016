﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

namespace Tests
{
    public class NetworkToolCanvasPainter : NetworkBehaviour
    {

        //ToolCanvasPainter tool;
        MeshDrawingCanvas canvas;

        public class StrokeDataMessage : MessageBase
        {
            public string test;
            public uint canvasId;
            public MarkerStrokeData[] data;
        }

        public void CreateStrokeOverNetwork(uint canvasId, MarkerStrokeData strokeData)
        {
            CmdCreateStroke(canvasId, strokeData);
        }

        public void AddSegmentOverNetwork(uint canvasId, uint ownerNetId, int strokeId, Vector3 localPoint, Vector3 localNormal)
        {
            CmdAddSegment(canvasId, ownerNetId, strokeId, localPoint, localNormal);
        }

        public void UndoStroke(uint canvasId, uint ownerNetId, int strokeId)
        {
            CmdUndoStroke(canvasId, ownerNetId, strokeId);
        }

        void Start()
        {
            //tool = GetComponent<ToolCanvasPainter>();
            //canvas = tool.canvas as MeshDrawingCanvas;

            if (!isLocalPlayer)
                return;

            //NetworkManager.singleton.client.RegisterHandler((short)(1000 + netId.Value), OnReceiveMyNetMessage);
            //Invoke("RequestStrokeDataFromServer", 0.3f);
        }

        void RequestStrokeDataFromServer()
        {
            //CmdRequestStrokeData(?, netId.Value);
        }

        void OnReceiveMyNetMessage(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<StrokeDataMessage>();

            Debug.Log(msg.test);

            var meshCanvas = CanvasManager.Find(msg.canvasId) as MeshDrawingCanvas;
            meshCanvas.Make(msg.data);
        }

        [Command]
        void CmdRequestStrokeData(uint canvasId, uint clientNetId)
        {
            var msg = new StrokeDataMessage();

            msg.test = "Got stroke data for netId:" + clientNetId.ToString();

            var meshCanvas = CanvasManager.Find(msg.canvasId) as MeshDrawingCanvas;

            msg.data = meshCanvas.GetData();

            Debug.Log("Server sending "+msg.data.Length+" strokes");

            Debug.Log("Server: sending message " + msg.test);

            // TODO: currently it's limited to 1400 bytes per msg, so it needs to be reworked so that stroke data is recieved by packets.
            // Covert data structure to byre array (http://unitydevelopers.blogspot.ru/2014/07/convert-structure-to-byte-array-in-c.html) and send that array by small pieces
            base.connectionToClient.Send((short)(1000 + clientNetId), msg);
        }

        [Command]
        void CmdCreateStroke(uint canvasId, MarkerStrokeData strokeData)
        {
            RpcCreateStroke(canvasId, strokeData);
        }

        [ClientRpc]
        void RpcCreateStroke(uint canvasId, MarkerStrokeData strokeData)
        {
            var meshCanvas = CanvasManager.Find(canvasId) as MeshDrawingCanvas;
            meshCanvas.CreateStroke(strokeData);
        }

        // Unity 5.3.0f4 Doesn't run when this method is in place.
        /*
        [Command]
        void CmdTest()
        {

        }
        */

        // Unity 5.3.0f4 doesn't run when there's an additional argument to this method
        [Command]
        void CmdAddSegment(uint canvasId, uint ownerNetId, int strokeId, Vector3 localPoint, Vector3 localNormal)
        {
            RpcAddSegment(canvasId, ownerNetId, strokeId, localPoint, localNormal);
        }

        [ClientRpc]
        void RpcAddSegment(uint canvasId, uint ownerNetId, int strokeId, Vector3 localPoint, Vector3 localNormal)
        {
            CanvasManager.Find(canvasId).GetComponent<NetworkMeshDrawingCanvas>().RequestAddSegment(ownerNetId, strokeId, localPoint, localNormal);
        }

        [Command]
        void CmdUndoStroke(uint canvasId, uint ownerNetId, int strokeId)
        {
            RpcUndoStroke(canvasId, ownerNetId, strokeId);
        }

        [ClientRpc]
        void RpcUndoStroke(uint canvasId, uint ownerNetId, int strokeId)
        {
            var meshCanvas = CanvasManager.Find(canvasId) as MeshDrawingCanvas;
            meshCanvas.RequestUndoStroke(ownerNetId, strokeId);
        }

    }
}
