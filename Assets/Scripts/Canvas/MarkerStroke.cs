﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Tests
{
    // Doesn't work with SyncListStruct because of a bug which prevents from having arrays: http://issuetracker.unity3d.com/issues/invalidprogramexception-invalid-il-code-when-using-syncliststruct-where-each-element-also-has-an-array
    // Data that can be used for re-constructing strokes from srachch.
    [System.Serializable]
    public struct MarkerStrokeData
    {
        public uint ownerId;
        public int id;
        public float thickness;
        public Vector3[] points;
        public Vector3[] normals;
    }

    public struct StrokeSegmentData
    {
        public Vector3 point;
        public Vector3 normal;
    }

    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MeshFilter))]
    public class MarkerStroke : MonoBehaviour
    {
        public float thickness = 0.3f;
        public MarkerStrokeData data;

        Vector3 facingDirection = Vector3.forward;
        List<Vector3> points = new List<Vector3>();
        List<Vector3> normals = new List<Vector3>();
        List<Vector3> thicknessPoints = new List<Vector3>();
        List<Vector3> vertices = new List<Vector3>();
        Mesh mesh;

        public static MarkerStroke CreateStroke(MarkerStrokeData data)
        {
            var stroke = new GameObject("stoke", typeof(MarkerStroke)).GetComponent<MarkerStroke>();
            stroke.data = data;
            stroke.points = new List<Vector3>(data.points);

            return stroke;
        }

        public Vector3[] GetPoints()
        {
            return points.ToArray();
        }

        /// <summary>
        /// Add a segment to a stroke either from a global or local position
        /// </summary>
        /// <param name="point">A stroke's point</param>
        /// <param name="fromLocal">If added from a local space related to the tranform</param>
        public void AddSegment(Vector3 point, Vector3 normal, bool fromLocal = false)
        {
            var targetPoint = fromLocal == true ? point : transform.InverseTransformPoint(point);
            var targetlNormal = fromLocal == true ? normal : transform.InverseTransformVector(normal);

            points.Add(targetPoint);
            normals.Add(targetlNormal);

            data.points = points.ToArray();
            data.normals = normals.ToArray();

            UpdateMesh();
        }

        public void UpdateMesh()
        {
            facingDirection = transform.parent != null ? transform.parent.forward : transform.forward;  

            if (points.Count < 2)
                return;

            UpdatePoints();

            var numTriangles = (points.Count - 1) * 2;
            var triangles = new int[numTriangles * 3];

            for (var s = 1; s < points.Count; s++)
            {
                int vA, vB, vC, vD;
                // An index of a first thickness point at a previous line point/segment.
                var prevFirstSegVertIndex = s * 2 - 2;

                prevFirstSegVertIndex = Mathf.Clamp(prevFirstSegVertIndex, 0, prevFirstSegVertIndex);

                vA = prevFirstSegVertIndex;
                vB = prevFirstSegVertIndex + 1;
                vC = s * 2 + 1;
                vD = s * 2;

                var ti = (s - 1) * 6;

                triangles[ti] = vA;
                triangles[ti + 1] = vB;
                triangles[ti + 2] = vC;

                triangles[ti + 3] = vA;
                triangles[ti + 4] = vC;
                triangles[ti + 5] = vD;
            }

            if (mesh == null)
            {
                mesh = GetComponent<MeshFilter>().mesh;
            }

            mesh.Clear();
            mesh.vertices = thicknessPoints.ToArray();
            mesh.triangles = triangles;
        }

        void UpdatePoints()
        {
            Vector3 a = Vector3.zero;
            Vector3 b = Vector3.zero;
            Vector3 c = Vector3.zero;
            Vector3 normal = Vector3.zero;
            Vector3[] vectors = new Vector3[2];

            vertices.Clear();
            thicknessPoints.Clear();

            for (var i = 0; i < points.Count; i++)
            {
                normal = normals[i];

                // A first point on the line.
                if (i == 0)
                {
                    a = points[0];
                    b = points[1];
                    vectors = GetTwoCrossPoints(a, b, normal, thickness);
                }
                // Any point that is not the first or the last one.
                else if (points.Count >= 3 && i > 0 && i < points.Count - 1)
                {
                    a = points[i - 1];
                    b = points[i];
                    c = points[i + 1];
                    vectors = GetTwoMidPoints(a, b, c, normal, thickness);
                }
                // A last point on the line.
                else if (i == points.Count - 1)
                {
                    a = points[points.Count - 1];
                    b = points[points.Count - 2];
                    vectors = GetTwoCrossPoints(a, b, -normal, thickness);
                }

                Debug.DrawLine(transform.TransformPoint(a), transform.TransformPoint(b), Color.grey);

                /*
                if (i > 0 && i < points.Count - 1)
                {
                    Debug.DrawLine(transform.TransformPoint(b), transform.TransformPoint(vectors[0]), Color.cyan);
                    Debug.DrawLine(transform.TransformPoint(b), transform.TransformPoint(vectors[1]), Color.magenta);
                }
                else
                {
                    Debug.DrawLine(transform.TransformPoint(a), transform.TransformPoint(vectors[0]), Color.cyan);
                    Debug.DrawLine(transform.TransformPoint(a), transform.TransformPoint(vectors[1]), Color.magenta);
                }
                */

                thicknessPoints.AddRange(vectors);
            }

        }

        /// <summary>
        /// Get two points that is a mid vector of "b-a" and "b-c" vectores.
        ///     p1
        ///     |
        /// a---b---c;
        ///     |
        ///     p0
        /// </summary>
        /// <param name="a">A point connected to "b"</param>
        /// <param name="b">A point that connects "a" and "c".</param>
        /// <param name="c">A point connected to "b"</param>
        /// <param name="thickness">A thickness of a mesh line</param>
        /// <returns></returns>
        Vector3[] GetTwoMidPoints(Vector3 a, Vector3 b, Vector3 c, Vector3 normal, float thickness = 1f)
        {
            var midPoints = new Vector3[2];
            var midThickness = thickness / 2f;
            var orthoVectorA = Vector3.Cross(b - a, -normal).normalized;
            var orthoVectorB = Vector3.Cross(c - b, -normal).normalized;
            var midVector = (orthoVectorA + orthoVectorB).normalized;

            midPoints[0] = b + midVector * this.thickness * 0.5f;
            midPoints[1] = b - midVector * this.thickness * 0.5f;

            return midPoints;
        }

        /// <summary>
        /// Get two points that are placed on a vector that cross a "a-b" and "facing" vectors.
        /// p1
        /// |
        /// a---b
        /// |
        /// p0
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="normal"></param>
        /// <param name="thickness"></param>
        /// <returns></returns>
        Vector3[] GetTwoCrossPoints(Vector3 a, Vector3 b, Vector3 normal, float thickness = 1f)
        {
            var orthoPoints = new Vector3[2];
            var midThickness = thickness / 2f;
            // Find a perpendicular vector to a and b (http://docs.unity3d.com/Manual/ComputingNormalPerpendicularVector.html)
            var orthoToAB = Vector3.Cross(b - a, -normal).normalized;

            orthoPoints[0] = a + orthoToAB * midThickness;
            orthoPoints[1] = a - orthoToAB * midThickness;

            return orthoPoints;
        }

    }
}
