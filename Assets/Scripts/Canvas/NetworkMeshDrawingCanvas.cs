﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

namespace Tests
{
    [RequireComponent(typeof(NetworkIdentity))]
    public class NetworkMeshDrawingCanvas : NetworkBehaviour
    {
        public List<MarkerStrokeData> strokesData = new List<MarkerStrokeData>();

        MeshDrawingCanvas canvas;

        void Start()
        {
            canvas.id = netId.Value;
        }

        void OnEnable()
        {
            canvas = GetComponent<MeshDrawingCanvas>();
            canvas.OnCreateStroke += CreateStroke;
            canvas.OnAddSegment += AddSegment;
            canvas.OnUndo += UndoStroke;
        }

        void OnDisable()
        {
            canvas.OnCreateStroke -= CreateStroke;
            canvas.OnAddSegment -= AddSegment;
            canvas.OnUndo -= UndoStroke;
        }

        void CreateStroke(MarkerStroke stroke)
        {
            stroke.data.ownerId = canvas.editor.GetComponent<NetworkBehaviour>().netId.Value;

            if (canvas.editor.GetComponent<NetworkIdentity>().isLocalPlayer)
            {
                canvas.editor.GetComponent<NetworkToolCanvasPainter>().CreateStrokeOverNetwork(canvas.id, stroke.data);
            }
        }

        void AddSegment(MarkerStroke stroke, Vector3 localPoint, Vector3 localNormal)
        {
            var ownerNetId = canvas.editor.GetComponent<NetworkBehaviour>().netId.Value;
            var strokeId = stroke.gameObject.GetInstanceID();

            canvas.editor.GetComponent<NetworkToolCanvasPainter>().AddSegmentOverNetwork(canvas.id, ownerNetId, strokeId, localPoint, localNormal);
        }

        public void RequestAddSegment(uint ownerNetId, int strokeId, Vector3 localPoint, Vector3 localNormal)
        {
			if (canvas.editor != null && ownerNetId == canvas.editor.GetComponent<NetworkBehaviour>().netId.Value)
                return;

            var stroke = canvas.strokes.Find(s => s.data.id == strokeId && s.data.ownerId == ownerNetId);

            if (stroke != null)
            {
                stroke.AddSegment(transform.TransformPoint(localPoint), transform.TransformVector(localNormal));
            }
            else
            {
                Debug.Log("Haven't found a stroke with id:"+strokeId+", ownerId:"+ownerNetId);
            }
        }

        public void UndoStroke(MarkerStroke stroke)
        {
            canvas.editor.GetComponent<NetworkToolCanvasPainter>().UndoStroke(canvas.id, stroke.data.ownerId, stroke.data.id);
        }

        void OnStrokesDataChange(SyncListStruct<MarkerStrokeData>.Operation op, int itemIndex)
        {
            Debug.Log("hey, stroke has changed");
            Debug.Log(itemIndex);
        }


    }
}
