﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Tests
{
    public class CanvasManager : SingletonComponent<CanvasManager>
    {
        public class CanvasList : List<DrawingCanvas>
        {
            public delegate void EventHandler(DrawingCanvas canvas);

            public event EventHandler OnAdd;
            public event EventHandler OnRemove;

            public new void Add(DrawingCanvas canvas)
            {
                if (canvas != null && OnAdd != null)
                {
                    Debug.Log("Added a new canvas");
                    OnAdd(canvas);
                }

                base.Add(canvas);
            }

            public new void Remove(DrawingCanvas canvas)
            {
                if (canvas != null && OnRemove != null)
                {
                    OnRemove(canvas);
                }

                base.Remove(canvas);
            }
        }

        public CanvasList canvases = new CanvasList();

        public static void Add(DrawingCanvas canvas)
        {
            Setup();

            Instance.canvases.Add(canvas);
        }

        public static void Remove(DrawingCanvas canvas)
        {
            Setup();

            Instance.canvases.Remove(canvas);
        }

        public static DrawingCanvas Find(uint canvasId)
        {
            if (Instance == null)
            {
                Debug.LogWarning("Canvas manager wasn't setup yet");
            }

            Setup();

            return Instance.canvases.Find(c => c.id == canvasId);
        }

    }
}

