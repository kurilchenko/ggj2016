﻿using UnityEngine;
using System.Collections;

public class FramerateLock : MonoBehaviour {

	public int defaultFramerate = 60;

	void Start () {
		Application.targetFrameRate = defaultFramerate;
	}

}
