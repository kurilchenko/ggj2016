﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public enum HoleSymbol 
{
	Symbol01,
	Symbol02,
	Symbol03
}

public class Hole : NetworkBehaviour
{
	public HoleSymbol symbol;
	public Animator right;
	public Animator wrong;

	public List<GameObject> objectsInside = new List<GameObject>();

	public bool isComplete;
	public bool containsStuff;

	void Start () 
	{
		if (!isServer)
			return;

		GetComponent<EnterExitSensor> ().OnSensorEnter += OnSensorEnter;
		GetComponent<EnterExitSensor> ().OnSensorExit += OnSensorExit;
		GetComponent<EnterExitSensor> ().registerNamesTagsTypes.Add ("Obtainable");
	}

	void Update () 
	{
		for (var i = 0; i < objectsInside.Count; i++) {
			if (objectsInside [i].transform.GetComponentInParent<SixenseHand> () == null) {
				// Object is insid and a hand doesn't hold it.
				var artifact = objectsInside[i].GetComponent<Artifact>();

				if (artifact != null) {
					if (symbol == artifact.symbol) {
						if (!isComplete) {
							SetCorrect ();
						}

						isComplete = true;
					} else {
						if (!containsStuff) {
							SetIncorrect ();
						}

						isComplete = false;
					}
				} else {
					isComplete = false;				
				}
			}
		}

		if (objectsInside.Count == 0) {
			isComplete = false;
			containsStuff = false;
		} else {
			containsStuff = true;
		}
	}

	void SetCorrect()
	{
		right.SetTrigger ("true");
	}

	void SetIncorrect()
	{
		wrong.SetTrigger ("true");
	}

	void OnSensorEnter(GameObject target)
	{
		objectsInside.Remove (target);
		objectsInside.Add (target);
	}

	void OnSensorExit(GameObject target)
	{
		objectsInside.Remove (target);
	}

}
