﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NetworkHole : NetworkBehaviour
{

	[SyncVar]
	bool syncIsComplete;
	[SyncVar]
	bool syncContainsStuff;

	void Update()
	{
		if (isServer) {
			syncIsComplete = GetComponent<Hole> ().isComplete;
			syncContainsStuff = GetComponent<Hole> ().containsStuff;
		}
	}


}
