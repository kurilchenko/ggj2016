﻿using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class GameOfStones : NetworkBehaviour
{
	public int maxPoints = 3;
	public int points;

	public Hole[] holes;
	public Artifact[] artifacts;
	public ArtifactPoint[] artifactPoints;
	public SymbolLarge[] symbols;
	public Transform[] symbolHolePositions;

	public bool isComplete;

	Transform container;

	public void Reset()
	{
		if (!isServer)
			return;

		var i = 0;

		RandomizeArray (artifacts);

		for (i = 0; i < artifacts.Length; i++) {
			holes [i].symbol = artifacts [i].symbol;
		}

		RandomizeArray (artifacts);

		for (i = 0; i < artifacts.Length; i++) {
			artifacts [i].transform.position = artifactPoints [i].transform.position;
			artifacts [i].GetComponent<Rigidbody> ().velocity = Vector3.zero;
			artifacts [i].GetComponent<Rigidbody> ().angularVelocity = Vector3.zero;
		}

		for (i = 0; i < holes.Length; i++) {
			var symbol = symbols.ToList ().Find (s => s.symbol == holes [i].symbol);
			symbol.transform.position = symbolHolePositions [i].position;
		}
	}

	public void Reward()
	{
		Debug.Log ("Congrats");
		
		RpcReward ();
	}

	[ClientRpc]
	void RpcReward()
	{
		foreach (var hand in FindObjectsOfType<ToolObjectGrabber>()) {
			hand.DropTarget ();
		}

		container.gameObject.SetActive (false);

		Debug.Log("Rotate the table to the game of lines");
		transform.parent.Rotate (new Vector3 (160f, 0, 0));
	}
		
	void RandomizeArray(Artifact[] arr)
	{
		for (var i = arr.Length - 1; i > 0; i--) {
			var r = Random.Range(0,i);
			var tmp = arr[i];
			arr[i] = arr[r];
			arr[r] = tmp;
		}
	}

	void Start()
	{
		container = GetComponentInParent<Transform> ();

		if (!isServer)
			return;

		Reset ();
	}

	void Update()
	{
		points = 0;

		for (var i = 0; i < holes.Length; i++) {
			points = holes[i].isComplete ? points + 1 : points;
		}

		if (points >= maxPoints) {
			Reward ();
		}
	}

}
