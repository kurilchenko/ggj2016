using UnityEngine;
using System.Collections;

public class SingletonComponent<T> : MonoBehaviour where T : MonoBehaviour
{
    protected bool isShuttingDown;

    static T instance;

    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = (T)FindObjectOfType(typeof(T));
            }

            return instance;
        }
    }

    public static void Setup()
    {
        if (instance != null)
            return;

        instance = new GameObject("~"+ typeof(T).ToString(), typeof(T)).GetComponent<T>();
    }

    void OnApplicationQuit()
    {
        isShuttingDown = true;
    }

}