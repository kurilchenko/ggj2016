﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Collider))]
public class EnterExitSensor : MonoBehaviour
{
	public delegate void OnSensorEvent(GameObject target);
	public event OnSensorEvent OnSensorEnter;
	public event OnSensorEvent OnSensorExit;

    public bool registerAll;
    public List<string> registerNamesTagsTypes = new List<string>();
    public bool triggersEnter = true;
    public bool triggersExit = true;
    public bool dispatchProcessing;

    List<System.Type> registedTypes = new List<System.Type>();

	public bool CheckAgainstRegistred(GameObject target)
    {
        return registerNamesTagsTypes.Find(s => s == target.tag || s == target.name || IsSameOrSubclass(target, s)) != null;
    }

    public bool IsSameOrSubclass(object target, string potentialTypeName)
    {
        var targetType = target.GetType();
        var potentialType = System.Type.GetType(potentialTypeName);

        if (potentialType == null)
            return false;

        return potentialType.IsAssignableFrom(targetType) || targetType == potentialType;
    }

    void Start()
    {
        GetComponent<Collider>().isTrigger = true;
    }

    void OnTriggerEnter(Collider other)
    {
        ProcessSensorWith(other, true);
    }

    void OnTriggerExit(Collider other)
    {
        ProcessSensorWith(other, false);
    }

	protected virtual GameObject ProcessSensorWith(Collider other, bool entering)
    {
        if (!triggersEnter)
            return null;

        if (other.attachedRigidbody == null)
            return null;

        // Get presumably a general Thing, not some of its children.
		var target = other.attachedRigidbody.gameObject;

        if (registerAll || CheckAgainstRegistred(target))
        {
			if (entering && OnSensorEnter != null) {
				OnSensorEnter.Invoke (target);
			} else if (!entering && OnSensorExit != null) {
				OnSensorExit.Invoke (target);
			}

            return target;
        }
        else
            return null;

        //return target;
    }
}
