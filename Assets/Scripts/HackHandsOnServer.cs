﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class HackHandsOnServer : NetworkBehaviour
{
    public GameObject handsPrefab;

    void Start()
    {
		if (isServer && isLocalPlayer) {
			var handsGO = FindObjectOfType<SixenseHandsController> ().gameObject;
			var tool = gameObject.AddComponent<Tests.StemDrawingTool> ();
			tool.hands = handsGO.GetComponentsInChildren<HackDrawingHand> ();
			tool.canvas = FindObjectsOfType<Tests.DrawingCanvas> ();

			GetComponent<Visitor> ().cameraRig.GetComponentInChildren<Camera> ().enabled = false;
			GetComponent<Visitor> ().cameraRig.GetComponentInChildren<AudioListener> ().enabled = false;

			Debug.Log ("Hide camera and stuff for" + gameObject);
		} 

		if (isServer && !isLocalPlayer) 
		{
			GetComponent<Visitor> ().cameraRig.GetComponentInChildren<Camera> ().enabled = true;
			GetComponent<Visitor> ().cameraRig.GetComponentInChildren<AudioListener> ().enabled = true;
		}

		if(!isServer && isLocalPlayer) 
		{
			FindObjectOfType<SixenseHandsController> ().enabled = false;

			foreach (var sixsenseHand in FindObjectsOfType<SixenseHand>()) {
				sixsenseHand.enabled = false;
			}

			foreach (var hackDraw in FindObjectsOfType<HackDrawingHand>()) {
				hackDraw.enabled = false;
			}

			GetComponent<Visitor> ().cameraRig.GetComponentInChildren<Camera> ().enabled = true;
			GetComponent<Visitor> ().cameraRig.GetComponentInChildren<AudioListener> ().enabled = true;

			Debug.Log ("Disable STEM controll for " + gameObject);
		}

    }

}
