﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VisitorSpotManager : MonoBehaviour
{
    public List<VisitorSpot> spots = new List<VisitorSpot>();

    public void AddVisitor(Visitor visitor)
    {
        var spot = spots.Find(s => s.visitor == null);

        if (spot == null)
            return;

        spot.SetVisitor(visitor);
    }
	
}
