﻿using UnityEngine;
using System.Collections;

public class NetworkVisitorSpotManager : NetworkBehaviourPlus
{

    VisitorSpotManager spotManager;

    protected override void Start()
    {
        base.Start();

        spotManager = GetComponent<VisitorSpotManager>();
    }

    protected override void NetworkUpdate()
    {
        base.NetworkUpdate();

        if (!isServer)
            return;

        foreach (var spot in spotManager.spots)
        {
            if (spot.visitor == null)
                continue;

            spot.visitor.GetComponent<NetworkVisitor>().RpcSetTransformInstantly(spot.transform.position, spot.transform.rotation, spot.transform.localScale);
        }
    }

}
