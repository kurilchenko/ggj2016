﻿using UnityEngine;
using System.Collections;

public class VisitorSpot : MonoBehaviour
{
    public Color color = Color.blue;

    public Visitor visitor { get; private set; }

    public void SetVisitor(Visitor visitor)
    {
        this.visitor = visitor;

        visitor.transform.position = transform.position;
        visitor.transform.rotation = transform.rotation;

        /*
        if (visitor.GetComponent<LaserPointer>())
        {
            visitor.GetComponent<LaserPointer>().color = color;
        }
        */
    }


	void OnDrawGizmos()
    {
        Gizmos.color = color;

        Gizmos.DrawWireSphere(transform.position + Vector3.up, 0.5f);
    }

}
