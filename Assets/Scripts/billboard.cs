﻿using UnityEngine;
using System.Collections;

public class Billboard : MonoBehaviour {
    //[ExecuteInEditMode]
    // Use this for initialization
    public Camera cam;
	void Start () {
        cam = Camera.main;
    }
	
	// Update is called once per frame
	void Update () {
		if (cam == null)
			return;

        cam = Camera.main;
        transform.LookAt(cam.transform);
    }
}
